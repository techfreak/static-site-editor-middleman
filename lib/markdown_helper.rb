# custom_markdown_extensions.rb
module MarkdownHelper
	require "middleman-core"
	require "middleman-core/renderers/redcarpet"
	#require 'middleman-core/renderers/redcarpet'
	class MyRenderer < Middleman::Renderers::MiddlemanRedcarpetHTML
		def initialize(options={})
			super
		end
		def paragraph(text)
		process_custom_tags("<p>#{text.strip}</p>\n")
	  end
	end
end

private 
def process_custom_tags(text)
		# Youtube videos
	if t = text.match(/(\[youtube-video )(.+)(\])/)
		youtube_video(t[2])
				# Soundcloud tracks
	elsif t = text.match(/(\[)(intro_card )(.+)(\])/)
		intro_card(t[2],"1","2","3")
				# Spotify albums
	elsif t = text.match(/(\[)(spotify)( )(album)( )(.+)(\])/)
		spotify_resource("album", t[6])
				# Spotify playlist, resource should be in the form of: username/playlist/playlist_id  
	elsif t = text.match(/(\[)(spotify)( )(playlist)( )(.+)(\])/)
		spotify_resource("playlist", t[6])
				# Spotify tracks  
	elsif t = text.match(/(\[)(spotify)( )(track)( )(.+)(\])/)
		spotify_resource("track", t[6])
				# if no match is found, just return the text    
	else 
		return text
	end
end
def youtube_video(resource_id)
	return <<-EOL
<p>
      <div class="youtube-video">
      <iframe width="100%" height="100%" src="https://www.youtube.com/embed/#{resource_id}"  \ 
      frameborder="0" allow="autoplay; encrypted-media" allowfullscreen>
      </iframe>
      </div>
      </p>
      EOL
  end
  def intro_card(title,description,txt,url)
    return <<-EOL
        <div class="content span-5">
        <h2 class="major">#{title}</h2>
        <p>
        #{description}
        Mauris a cursus velit. Nunc lacinia sollicitudin egestas bibendum, magna dui bibendum ex, sagittis commodo enim risus sed magna nulla. Vestibulum ut consequat velit. Curabitur vitae libero lorem. Quisque iaculis porttitor blandit. Nullam quis sagittis maximus. Sed vel nibh libero. Mauris et lorem pharetra massa lorem turpis congue pulvinar.</p>
        </div>
        <div class="#{txt}" data-position="top right">
        <img src="images/#{url}" alt="#{txt}" />
        </div>
        </section>

        EOL
    end
def soundcloud_resource(resource_id)
	return <<-EOL
<div class="soundcloud-song">
      <iframe width="100%" height="300" scrolling="no" \
       frameborder="no" allow="autoplay" \
       src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/#{resource_id} \ 
       &color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true& \ 
       show_reposts=false&show_teaser=true&visual=true"> \ 
       </iframe>
      </div>
      EOL
  end
def spotify_resource(resource_type, resource_id)
		if resource_type == "album"
		return <<-EOL
		<div class="spotify-container">
        <iframe src="https://open.spotify.com/embed/album/#{resource_id}"  \ 
        width="100%" height="100%" frameborder="0" allowtransparency="true"></iframe>
        </div>
        EOL
    elsif resource_type == "track"
		return <<-EOL
		<div class="spotify-container">
        <iframe src="https://open.spotify.com/embed/track/#{resource_id}" \
         width="100%" height="100%" frameborder="0" allowtransparency="true"></iframe>
        </div>
        EOL
    elsif resource_type == "playlist"
		return <<-EOL
	<div class="spotify-container">
        <iframe src="https://open.spotify.com/embed/user/#{resource_id}" \
         width="100%" height="100%" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
        </div>
        EOL
    end
end
